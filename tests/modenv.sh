# Emulate environment of the usbkey as it would be for modules. You have to load
# modules manually in the tests.
set -u

register_operation() { true; }

fatal() {
	echo "${0##*/}:" "$@" >&2
	exit 1
}

internal_error() {
	fatal "Internal error:" "$@"
}

notice() {
	echo "$@" >&2
}

debug() {
	echo "$@" >&2
}

automount() {
	true # dummy implementation
}

autoumount() {
	true # dummy implementation
}
