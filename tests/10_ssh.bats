#!/usr/bin/env bats
load modenv.sh
bats_require_minimum_version "1.7.0"

setup() {
	source "${BATS_TEST_FILENAME%/*}/../modules/02_git.sh"
	source "${BATS_TEST_FILENAME%/*}/../modules/10_ssh.sh"
	export mount_path="$BATS_TEST_TMPDIR"
	git_initialize_repository
}

@test "generate" {
	op_ssh -c comment -n some
	[[ -f "$mount_path/ssh/some" ]]
	[[ -f "$mount_path/ssh/some.pub" ]]
}
