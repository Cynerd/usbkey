#!/usr/bin/env bats
load modenv.sh
bats_require_minimum_version "1.7.0"

setup() {
	source "${BATS_TEST_FILENAME%/*}/../modules/02_git.sh"
	export mount_path="$BATS_TEST_TMPDIR"
}

@test "init" {
	git_initialize_repository
	[ -d "$mount_path/.git" ]
}

@test "commit_changes" {
	git_initialize_repository
	echo "Some content" >"$mount_path/foo"
	git_commit_changes "Test message" "$mount_path" "foo"

	git -C "$mount_path" diff --quiet HEAD
	run -0 git -C "$mount_path" log --format=%B -n 1 HEAD
	[ "$output" = "Test message" ]
}

repo_push_check() {
	local testrepo="$1"
	local mount_path="$2"
	git_push_repo "TestRepo" "$testrepo"
	[ ! -f "$mount_path/footest" ]
	[ "$(git_repo_hash "TestRepo")" = "$(git -C "$testrepo" rev-parse HEAD)" ]
}
@test "repo_push" {
	# Prepare repostory to be pushed to the usbkey
	testrepo="$mount_path/testrepo"
	mkdir -p "$testrepo"
	git -C "$testrepo" init
	echo "Testing content" >"$testrepo/footest"
	git -C "$testrepo" add footest
	git -C "$testrepo" commit -m "Test repo commit one" "footest"

	# Now merge it to our repository
	mount_path="$mount_path/repo"
	mkdir -p "$mount_path"
	git_initialize_repository
	repo_push_check "$testrepo" "$mount_path"

	# Add new commit and try it again
	echo "Newer content" >"$testrepo/footest"
	git -C "$testrepo" commit -m "Test repo commit two" "footest"
	repo_push_check "$testrepo" "$mount_path"
}

# TODO implements tests for repository synchronization
