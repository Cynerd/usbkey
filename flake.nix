{
  description = "USBKey is the script that manages keys on encrypted drive.";

  outputs = {
    self,
    flake-utils,
    nixpkgs,
  }: let
    inherit (flake-utils.lib) eachDefaultSystem filterPackages;
    inherit (nixpkgs.lib) licenses platforms makeBinPath;

    packages = pkgs: let
      deps = with pkgs; [
        gnused
        gawk
        # Mount module
        util-linux
        cryptsetup
        # Git module
        git
        tig
        gitg
        # Format module
        exfatprogs
        # CA module
        openssl
        # SHV module
        openssh
        # GPG module
        gnupg
        # Wireguard
        wireguard-tools
        # Syncthing module
        syncthing
      ];
    in {
      usbkey = pkgs.stdenvNoCC.mkDerivation rec {
        pname = "usbkey";
        version = "1.0.0";
        src = ./.;
        nativeBuildInputs = with pkgs; [
          makeWrapper
          installShellFiles
        ];
        # TODO possibly allow setting list of module paths
        installPhase = ''
          mkdir -p $out/bin
          makeShellWrapper ${./usbkey} $out/bin/usbkey \
            --prefix PATH : ${makeBinPath deps} \
            --add-flags '-m ${./modules}'
          #installShellCompletion --bash --name usbkey bash_completions
          #installShellCompletion --zsh --name _usbkey zsh_completions
        '';
        meta = {
          description = "Script to manage keys on the USB device.";
          homepage = "https://git.cynerd.cz/usbkey";
          license = licenses.gpl3Plus;
          platforms = platforms.linux;
        };
      };
    };
  in
    {
      nixosModules = rec {
        usbkey = import ./nixos_module.nix self.overlays.default;
        default = usbkey;
      };
      overlays.default = final: prev: packages prev;
    }
    // eachDefaultSystem (system: let
      pkgs = nixpkgs.legacyPackages.${system}.extend self.overlays.default;
    in {
      packages = {
        inherit (pkgs) usbkey;
        default = pkgs.usbkey;
      };
      legacyPackages = pkgs;

      devShells = filterPackages system {
        default = pkgs.mkShell {
          packages = with pkgs; [
            bats
            expect
            shellcheck
            shfmt
            editorconfig-checker
          ];
          inputsFrom = [pkgs.usbkey];
          meta.platforms = nixpkgs.lib.platforms.linux;
        };
      };
      formatter = nixpkgs.legacyPackages.${system}.alejandra;
    });
}
