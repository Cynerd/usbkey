# Implementation of common cleanup handlers
declare -a cleanup_funcs

add_cleanup() {
	local func="$1"
	cleanup_funcs+=("$func")
}

rm_cleanup() {
	local func="$1"
	cleanup_funcs=("${cleanup_funcs[@]/$func/}")
}

perform_cleanup() {
	local func="$1"
	rm_cleanup "$func"
	"$func"
}

exit_cleanup() {
	for func in "${cleanup_funcs[@]}"; do
		"$func"
	done
	cleanup_funcs=()
}
trap exit_cleanup EXIT
