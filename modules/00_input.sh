# Helper functions for input

# Read from terminal until input from the user matches given regexp.
# regexp: extended regular expression that input should match
# prompt: prompt to display to the user
# format: description of the format expected
readmatch() {
	local regexp="$1"
	local prompt="$2"
	local format="$3"
	local value
	while true; do
		read -p "$prompt" -r value
		if echo "$value" | grep -qE "$regexp"; then
			break
		else
			echo "$format"
		fi
	done
	echo "$value"
}
