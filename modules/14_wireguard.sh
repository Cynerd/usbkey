# Wireguard keys

# List all available wireguard devices
wireguard_list() {
	find "$mount_path/wireguard" -type d 2>/dev/null | while read -r dir; do
		[[ -f "$dir/key" ]] || continue
		[[ -f "$dir/pub" ]] || continue
		echo "${dir#"$mount_path/wireguard/"}"
	done
}

# Generate private key for the new wireguard device
wireguard_new() {
	local device="${1:-}"
	command -v wg >/dev/null \
		|| fatal "Wireguard tools have to be available to generate the new key"
	[[ -n "$device" ]] \
		|| device="$(readmatch '^.+$' "Device name: " "Device name has to be nonempty.")"

	local pth="$mount_path/wireguard/$device"
	mkdir -p "$pth"
	wg genkey >"$pth/key"
	wg pubkey <"$pth/key" >"$pth/pub"
	# Note: we generate and save public key although it can be received later on
	# to allow get on systems without wireguard-tools (such as for deployment).

	git_commit_changes "Added Wireguard device '$device'" "$mount_path" \
		"$pth"/{key,pub}
}

wireguard_new_psk() {
	local device="$1"
	local group="$2"
	command -v wg >/dev/null \
		|| fatal "Wireguard tools have to be available to generate the new pre-shared key"
	local pth="$mount_path/wireguard/$device"
	[[ -f "$pth/key" ]] \
		|| fatal "The device '$device' does not have key. Generate the key first!"

	wg genpsk >"$pth/$group.psk"

	git_commit_changes "Added Wireguard pre-shared key for device '$device' in group '$group'" \
		"$mount_path" "$pth/$group.psk"
}

_wireguard_get_generic() {
	local device="$1"
	local file="$2"
	local pth="$mount_path/wireguard/$device/$file"
	[[ -f "$pth" ]] || fatal "No such path: $pth"
	cat "$pth"
}
wireguard_key() {
	_wireguard_get_generic "$1" "key"
}
wireguard_pub() {
	_wireguard_get_generic "$1" "pub"
}
wireguard_psk() {
	_wireguard_get_generic "$1" "$2.psk"
}

# Wireguard operation ##########################################################
op_wireguard_usage() {
	echo "Usage: usbkey wireguard [OPTION].. [DEVICE].." >&2
}

op_wireguard_help() {
	op_wireguard_usage
	cat >&2 <<-EOF
		Keys for Wireguard endpoints. In default it prints all keys for the device.

		Options:
		  -l       List available devices
		  -s       Print only private (secret) key for the device
		  -p       Print only public key for the device
		  -e       Print only pre-shared key for the device and group
		  -n       Generate keys for the new device or new pre-shared key if -g is specified
		  -g GRP   Set group for pre-shared keys
	EOF
}

op_wireguard() {
	local list="n"
	local new="n"
	local group=""
	local print="all"
	while getopts "lspeng:h" opt; do
		case "$opt" in
		l)
			list="y"
			;;
		s)
			print="private"
			;;
		p)
			print="public"
			;;
		e)
			print="psk"
			;;
		n)
			new="y"
			;;
		g)
			[[ -n "$OPTARG" ]] || fatal "Group has to be non-empty"
			group="$OPTARG"
			;;
		h)
			op_wireguard_help
			exit 0
			;;
		*)
			op_wireguard_usage
			exit 2
			;;
		esac
	done
	shift $((OPTIND - 1))

	automount fatal

	if [[ "$list" = "y" ]]; then
		wireguard_list
	elif [[ "$new" = "y" ]]; then
		if [[ -n "$group" ]]; then
			# TODO chec that there is $1 argument
			[[ -d "$mount_path/wireguard/$1" ]] \
				|| fatal "No such key: $1"
			wireguard_new_psk "${1:-}" "$group"
		else
			[[ -f "$mount_path/wireguard/$1" ]] \
				&& fatal "Key already exists: $1"
			# TODO for all arguments
			wireguard_new "${1:-}"
		fi
	else
		# TODO error if there is no such file
		case "$print" in
		"private")
			wireguard_key "$1"
			;;
		"public")
			wireguard_pub "$1"
			;;
		"psk")
			wireguard_psk "$1" "$group"
			;;
		esac
	fi

	autoumount
}

register_operation "wireguard" op_wireguard \
	"Keys for Wireguard endpoints"
