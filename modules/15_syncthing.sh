# Syncthing client certificates
syncthing_default_dir="$HOME/.config/syncthing"

syncthing_list() {
	find "$mount_path/syncthing" -type d 2>/dev/null | while read -r dir; do
		[[ -f "$dir/cert.pem" ]] || continue
		[[ -f "$dir/key.pem" ]] || continue
		[[ -f "$dir/deviceid" ]] || continue
		echo "${dir#"$mount_path/syncthing/"}"
	done
}

syncthing_new() {
	local name="${1:-}"
	command -v syncthing >/dev/null \
		|| fatal "Syncthing application has to be available to generate the new key"
	[[ -n "$name" ]] \
		|| name="$(readmatch '^.+$' "Host name: " "Host name has to be nonempty.")"

	local pth="$mount_path/syncthing/$name"
	mkdir -p "$pth"
	syncthing generate --home="$pth"
	sed -n 's/.*device id="\([^"]\+\)".*/\1/p' "$pth/config.xml" | head -1 >"$pth/deviceid"
	rm -f "$pth/config.xml"

	git_commit_changes "Added Syncthing device '$name'" "$mount_path" \
		"$pth"/{cert.pem,key.pem,deviceid}
}

syncthing_import() {
	local name="${1:-}"
	if [[ -z "$name" ]]; then
		echo "Available devices:"
		syncthing_list
		while true; do
			read -p "Select by name: " -r name
			[[ -f "$mount_path/syncthing/$name" ]] && break
			notice "There is no device with name '$name'"
		done
	fi

	mkdir -p "$syncthing_home"
	cp "$mount_path/syncthing/$name"/{cert.pem,key.pem} "$syncthing_home/"
	chmod 600 "$syncthing_home"/{cert.pem,key.pem}
}

syncthing_deviceid() {
	local name="${1:-}"
	if [[ -z "$name" ]]; then
		echo "Available devices:"
		syncthing_list
		while true; do
			read -p "Select by name: " -r name
			[[ -f "$mount_path/syncthing/$name" ]] && break
			notice "There is no device with name '$name'"
		done
	fi
	cat "$mount_path/syncthing/$name/deviceid"
}

# Syncthing operation ##########################################################
op_syncthing_usage() {
	echo "Usage: usbkey syncthing [OPTION].. [DEVICE].." >&2
}

op_syncthing_help() {
	op_syncthing_usage
	cat >&2 <<-EOF
		Keys for syncthing devices. In default it imports keys.

		Options:
		  -l       List available devices
		  -c PATH  Syncthing's home directory (in default: $syncthing_default_dir)
		  -p       Print device's device ID instead of importing it
		  -n       Generate keys for the new device
	EOF
}

op_syncthing() {
	local list="n"
	local syncthing_home="$syncthing_default_dir"
	local action=syncthing_import
	while getopts "lcpnh" opt; do
		case "$opt" in
		l)
			list="y"
			;;
		c)
			syncthing_home="$OPTARG"
			;;
		p)
			action=syncthing_deviceid
			;;
		n)
			action=syncthing_new
			;;
		h)
			op_syncthing_help
			exit 0
			;;
		*)
			op_syncthing_usage
			exit 2
			;;
		esac
	done
	shift $((OPTIND - 1))

	automount fatal

	if [[ "$list" = "y" ]]; then
		syncthing_list
	else
		if [[ $# -gt 0 ]]; then
			for device in "$@"; do
				"$action" "$device"
			done
		else
			"$action"
		fi
	fi

	autoumount
}

register_operation "syncthing" op_syncthing \
	"Keys for syncthing hosts"
