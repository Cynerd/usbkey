# SSH keys

ssh_list() {
	find "$mount_path/ssh" -name '*.pub' 2>/dev/null | while read -r key; do
		n="${key#"$mount_path/ssh/"}"
		echo -n "${n%.pub}: "
		sed -n 's/ssh[^ ]* [^ ]* \(.*\)/\1/p' "$key"
	done
}

ssh_new() {
	local name="${1:-}"
	local comment="${2:-}"
	command -v ssh-keygen >/dev/null \
		|| fatal "SSH application 'ssh-keygen' has to be available to generate the new key"
	[[ -n "$name" ]] \
		|| name="$(readmatch '^.+$' "Key name: " "Name has to be nonempty.")"
	[[ -n "$comment" ]] \
		|| read -p "Comment: " -r comment

	mkdir -p "$mount_path/ssh"
	ssh-keygen -f "$mount_path/ssh/$name" -N '' -C "$comment"
	git_commit_changes "Added SSH key '$name'" "$mount_path" "ssh/$name"{,.pub}
}

_ssh_import() {
	local name="$1"
	cp "$mount_path/ssh/$name.pub" ~/.ssh/
	chmod 600 ~/.ssh/"$name.pub"
	[[ "$public" = "y" ]] || {
		cp "$mount_path/ssh/$name" ~/.ssh/
		chmod 600 ~/.ssh/"$name"
	}
}

ssh_import() {
	local name
	if [[ $# -gt 0 ]]; then
		for name in "$@"; do
			_ssh_import "$name"
		done
	else
		echo "Available keys:"
		ssh_list
		while true; do
			read -p "Select by name: " -r name
			[[ -f "$mount_path/ssh/$name" ]] && break
			notice "There is no key with name '$name'"
		done
		_ssh_import "$name"
	fi
}

# SSH operation ################################################################
op_ssh_usage() {
	echo "Usage: usbkey ssh [OPTION].. [KEY].." >&2
}

op_ssh_help() {
	op_ssh_usage
	cat >&2 <<-EOF
		SSH authentication keys. In default it imports keys to the ~/.ssh directory.

		Options:
		  -l      List available keys
		  -p      Import only public key (not private key)
		  -n      Generate new key instead of importing existing one
		  -c STR  Comment for the new key
	EOF
}

op_ssh() {
	local public="n"
	local action=ssh_import
	local comment=""
	while getopts "lpnc:h" opt; do
		case "$opt" in
		l)
			action=ssh_list
			;;
		p)
			public="y"
			;;
		n)
			action=ssh_new
			;;
		c)
			comment="$OPTARG"
			;;
		h)
			op_ssh_help
			exit 0
			;;
		*)
			op_ssh_usage
			exit 2
			;;
		esac
	done
	shift $((OPTIND - 1))

	automount fatal

	if [[ "$action" = "ssh_new" ]]; then
		# TODO more than one argument is error
		ssh_new "${1:-}" "$comment"
	else
		"$action" "$@"
	fi

	autoumount
}

register_operation "ssh" op_ssh \
	"SSH authentication key"
