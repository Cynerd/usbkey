# This is the sync operation

sync_devices() {
	local tmpdir
	tmpdir="$(mktemp -d)"
	local -a mountpaths
	for device in "${devices[@]}"; do
		local mntpath
		mntpath="$(mounted_path "$device" | head -1)"
		if [[ -z "$mntpath" ]]; then
			mntpath="$tmpdir/$(mapper4device "$device")"
			mkdir -p "$mntpath"
			device_mount "$device" "$mntpath"
		fi
		mountpaths+=("$mntpath")
	done
	[[ ${#mountpaths[@]} -gt 0 ]] || return 0
	# This functions does works correctly when there is at least one mounted
	# filesystem.

	git_sync_repositories "${mountpaths[@]}"

	# Umount only paths in our tmpdir
	for mntpath in "$tmpdir"/*; do
		destination_umount "$mntpath"
		rmdir "$mntpath"
	done
	rmdir "$tmpdir"

	notice "Sync process finished."
}

# Sync operation ###############################################################
op_sync_usage() {
	echo "Usage: usbkey sync" >&2
}

op_sync_usage_fail() {
	echo "usbkey:" "$@"
	op_sync_usage
	exit 2
}

op_sync_help() {
	op_sync_usage
	cat >&2 <<-EOF
		Syncronize changes between all connected devices.

		Options:
		  -h  Print this help text
	EOF
}

op_sync() {
	while getopts "h" opt; do
		case "$opt" in
		h)
			op_sync_help
			exit 0
			;;
		*)
			op_sync_usage_fail "Invalid option: $opt"
			;;
		esac
	done
	shift $((OPTIND - 1))
	[[ $# -eq 0 ]] || op_sync_usage_fail "No additional argument expected but got: $*"

	sync_devices
}

register_operation "sync" op_sync \
	"Syncronize multiple drives changes"
