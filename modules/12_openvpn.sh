# OpenVPN certificate configuration

openvpn_opessl_config() {
	local auth="$1"
	local name="$2"
	local mode="${3:-client}"
	local path="$mount_path/openvpn/$auth"
	local out
	out="$(mktemp --tmpdir usbkey-openvpn-conf-XXXXXX)"
	cat >"$out" <<-EOF
		[ ca ]
		default_ca = ca_default

		[ ca_default ]
		certs = $path
		new_certs_dir = $path
		database = $path/index.txt
		serial = $path/serial
		certificate = $path/ca.crt
		private_key = $path/ca.key
		crl_dir = $path
		crl = $path/crl.pem
		crlnumber = $path/crlnumber
		RANDFILE = $path/.rand

		default_md = default

		policy  = policy_match

		x509_extensions = v3_req

		[ policy_match ]
		countryName = optional
		stateOrProvinceName = optional
		organizationName = optional
		organizationalUnitName = optional
		commonName = supplied
		emailAddress = optional

		[ req ]
		default_bits        = 4096
		distinguished_name = req_distinguished_name

		string_mask = utf8only
		prompt = no

		x509_extensions = v3_ca

		[ req_distinguished_name ]
		commonName = $name

		[v3_ca]
		basicConstraints = critical,CA:true
		keyUsage = cRLSign, keyCertSign
		nsCertType = sslCA
		subjectKeyIdentifier=hash
		authorityKeyIdentifier=keyid:always,issuer
		issuerAltName=issuer:copy

	EOF
	case "$mode" in
	"server")
		cat >>"$out" <<-EOF
			[v3_req]
			basicConstraints = critical,CA:false
			keyUsage = digitalSignature, keyEncipherment
			nsCertType = server
			extendedKeyUsage = serverAuth
			subjectKeyIdentifier=hash
			authorityKeyIdentifier=keyid,issuer
			issuerAltName=issuer:copy
		EOF
		;;
	"client")
		cat >>"$out" <<-EOF
			[v3_req]
			basicConstraints = critical,CA:false
			keyUsage = digitalSignature, keyEncipherment
			nsCertType = client
		EOF
		;;
	esac
	echo "$out"
}

openvpn_auth_exists() {
	local auth="$1"
	ca_auth_exists "$mount_path/openvpn/$auth"
}

openvpn_auth_list() {
	for pth in "$mount_path/openvpn/"*; do
		local auth="${pth#$mount_path/openvpn/}"
		if openvpn_auth_exists "$auth"; then
			echo "$auth"
		fi
	done
}

openvpn_auth_generate() {
	local auth="$1"
	openvpn_auth_exists "$auth" \
		&& fatal "OpenVPN authority '$auth' already exists!"
	notice "Generating new OpenVPN authority: $auth"
	local path="$mount_path/openvpn/$auth"
	local config
	config="$(openvpn_opessl_config "$auth" "$auth" "ca")"
	mkdir -p "$path"
	touch "$path/index.txt"
	touch "$path/notes.txt"
	echo 01 >"$path/serial"
	echo 01 >"$path/crlnumber"
	ca_initialize "$config" "$mount_path/openvpn/$auth"
	rm -f "$config"
	git_commit_changes "Added OpenVPN authority '$auth'" "$mount_path" "openvpn/$auth"
}

openvpn_auth_delete() {
	local auth="$1"
	openvpn_auth_exists "$auth" \
		|| fatal "There is no such authority: $auth"
	rm -rf "$mount_path/openvpn/$auth"
	notice "OpenVPN authority '$auth' removed!"
	git_commit_changes "Removed OpenVPN authority '$auth'" "$mount_path" "openvpn/$auth"
}

openvpn_list() {
	local auth="$1"
	openvpn_auth_exists "$auth" \
		|| fatal "There is no such authority: $auth"
	sed -E '/unspecified/d;s/.*CN=//' "$mount_path/openvpn/$auth/index.txt"
}

_openvpn_new() {
	local mode="$1"
	local auth="$2"
	shift 2
	if ! openvpn_auth_exists "$auth"; then
		if [[ "$auth" == "default" ]]; then
			openvpn_auth_generate "$auth"
		else
			fatal "There is no such authority: $auth"
		fi
	fi
	[[ $# -gt 0 ]] \
		|| set -- "$(readmatch '^.+$' "Device name: " "Name has to be nonempty.")"
	for name in "$@"; do
		ca_exists "$mount_path/openvpn/$auth" "$name" \
			&& fatal "Device already exists, you must revoke it first."
	done

	local config
	for name in "$@"; do
		# TODO we can use -subj and move config outside of the loop
		config="$(openvpn_opessl_config "$auth" "$name" "$mode")"
		ca_new "$config" "$mount_path/openvpn/$auth" "$name"
		rm -f "$config"
	done
	git_commit_changes "Added OpenVPN device to authority '$auth': $*" "$mount_path" "openvpn/$auth"
}

openvpn_new() {
	_openvpn_new "client" "$@"
}

openvpn_new_server() {
	_openvpn_new "server" "$@"
}

openvpn_revoke() {
	local auth="$1"
	shift 1
	openvpn_auth_exists "$auth" \
		|| fatal "There is no such authority: $auth"
	if [[ $# -eq 0 ]]; then
		echo "Devices:"
		openvpn_list "$auth"
		set -- "$(readmatch '^.+$' "Device name: " "Name has to be nonempty.")"
	fi

	local config
	config="$(openvpn_opessl_config "$auth" "$name")"
	for name in "$@"; do
		ca_revoke "$config" "$mount_path/openvpn/$auth" "$name"
		rm -f "$mount_path/openvpn/$auth/$name".*
	done
	rm -f "$config"
	git_commit_changes "Revoked OpenVPN device in authority '$auth': $*" "$mount_path" "openvpn/$auth"
}

openvpn_import() {
	local auth="$1"
	local out="$2"
	shift 2
	openvpn_auth_exists "$auth" \
		|| fatal "There is no such authority: $auth"
	if [[ $# -eq 0 ]]; then
		echo "Devices:"
		openvpn_list "$auth"
		set -- "$(readmatch '^.+$' "Device name: " "Name has to be nonempty.")"
	fi

	for name in "$@"; do
		[[ "$out" == "." ]] && out="openvpn-$name"
		mkdir -p "$out"
		for file in "$name.key" "$name.crt" "ca.crt"; do
			cp "$mount_path/openvpn/$auth/$file" "$out/$file"
		done
		notice "OpenVPN key $name copied to $out directory." >&2
	done
}

# OpenVPN operation ############################################################
op_openvpn_usage() {
	echo "Usage: ${0##*/} openvpn [OPTION].. [DEVICE].." >&2
}

op_openvpn_help() {
	op_openvpn_usage
	cat >&2 <<-EOF
		Keys and authority for OpenVPN. In default it imports keys.

		Options:
		  -l       List existing device keys
		  -n       Generate certficate for the new device
		  -s       Generate server server certificate
		  -r       Revoke certificate
		  -o PATH  Output directory for the imported keys (current if not used)
		  -a AUTH  Authority to be used ("default" if not used)
		  -g       Generate authority if not exists yet ("default" gets generated without this)
		  -i       List all authorities
		  -d       Delete selected authority
	EOF
}

op_openvpn() {
	local action=openvpn_import
	local authority="default"
	local output="."
	while getopts "lnsro:a:gidh" opt; do
		case "$opt" in
		l)
			action=openvpn_list
			;;
		n)
			action=openvpn_new
			;;
		s)
			action=openvpn_new_server
			;;
		r)
			action=openvpn_revoke
			;;
		o)
			output="$OPTARG"
			;;
		a)
			authority="$OPTARG"
			;;
		g)
			action=openvpn_auth_generate
			;;
		i)
			action=openvpn_auth_list
			;;
		d)
			action=openvpn_auth_delete
			;;
		h)
			op_openvpn_help
			exit 0
			;;
		*)
			op_openvpn_usage
			exit 2
			;;
		esac
	done
	shift $((OPTIND - 1))

	automount fatal

	case "$action" in
	openvpn_import)
		openvpn_import "$authority" "$output" "$@"
		;;
	*)
		"$action" "$authority" "$@"
		;;
	esac

	autoumount
}

register_operation "openvpn" op_openvpn \
	"Keys and authorities for OpenVPN"
