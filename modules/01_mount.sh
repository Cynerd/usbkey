# This implements the primary operation of mounting USB keys
# This module uses primary arguments and configuration for devices and UUID.

# Convert list of UUIDs to present devices
for uuid in "${uuid_keys[@]}"; do
	if _device_uuid="$(blkid --uuid "$uuid")"; then
		devices+=("$_device_uuid")
	fi
done

mapper4device() {
	local device="$1"
	# TODO possibly cache the result
	# Note: we are not using blkid here as it does not provide UUID unless run
	# with root privileges.
	echo "usbkey-$(lsblk -l -o UUID "$device" | sed -n '2p')"
}

# Check where the device is mounted (returns all mounted paths)
mounted_path() {
	local device="$1"
	findmnt -nl -o TARGET "/dev/mapper/$(mapper4device "$device")" || true
}

# Check that device is mounted somewhere
device_is_mounted() {
	local device="$1"
	[[ -n "$(mounted_path "$device")" ]]
}

# Check which device is mounted on given path.
mounted_device() {
	local path="$1"
	shift
	local devs
	if [[ $# -gt 0 ]]; then
		devs=("$@")
	else
		devs=("${devices[@]}")
	fi
	for device in "${devs[@]}"; do
		if findmnt --source "/dev/mapper/$(mapper4device "$device")" --target "$path" >/dev/null; then
			echo "$device"
			return
		fi
	done
}

# Check that given path has one of the mounted devices (or all in default).
is_mounted() {
	local path="$1"
	shift
	[[ -n "$(mounted_device "$path" "$@")" ]]
}

_sudo() {
	if [[ "$(id -u)" -ne 0 ]]; then
		sudo -p "Sudo password: " -- "$@"
	else
		"$@"
	fi
}

# Mount usbkey device.
# device: the device to be mounted
# destination: destination directory where device should be mounted
device_mount() {
	local device="$1"
	local destination="$2"
	is_mounted "$destination" "$device" && return 0
	local mapper
	mapper="$(mapper4device "$device")"
	# Decrypt the drive
	if [[ ! -b "/dev/mapper/$mapper" ]]; then
		notice "Decrypting usb key: $device"
		_sudo cryptsetup open "$device" "$mapper"
	fi
	# Mount the drive
	notice "Mounting usb key: $device"
	_sudo mkdir -p "$destination"
	_sudo mount -o uid="$(id -u)",gid="$(id -g)" "/dev/mapper/$mapper" "$destination"

	notice "USB key drive '$device' mounted on: $destination"
}

# Umount usbkey device.
# device: umount the device
device_umount() {
	local device="$1"
	local mapper
	mapper="$(mapper4device "$device")"
	[[ -b "/dev/mapper/$mapper" ]] || return 0 # not unlocked
	# Umount mount pounts
	if findmnt "/dev/mapper/$mapper" >/dev/null; then
		echo "umount" "/dev/mapper/$mapper"
		_sudo umount "/dev/mapper/$mapper"
		sleep 1 # give some time to prevent device being busy
	fi
	# Close the device
	_sudo cryptsetup close "$mapper"
}

# Umount usbkey destination.
# destination: umount the destination
destination_umount() {
	local destination="$1"
	local device
	device="$(mounted_device "$destination")"
	[[ -n "$device" ]] || return 0 # no device means nothing to unmount
	# Umount the mount pount
	_sudo umount "$destination"
	# Close the device if this was the last mount point
	if ! findmnt "$device" >/dev/null; then
		sleep 1 # give some time to prevent device being busy
		_sudo cryptsetup close "$(mapper4device "$device")"
	fi
}

# Umount that detects if passed path is device and calls appropriate umount
# function.
generic_umount() {
	local path="$1"
	if [[ -b "$path" ]]; then
		device_umount "$path"
	else
		destination_umount "$path"
	fi
}

# Internal function for other modules to use.
# It mounts first available device to the $mount_path
# You can pass additional argument "fatal" to make sure that mount happens.
automount() {
	local mode="${1:-silent}"
	automount_used="n"
	is_mounted "$mount_path" && return 0
	if [[ "$automount" != "y" ]]; then
		case "$mode" in
		silent)
			return 1
			;;
		fatal)
			fatal "Automount disabled but mount is required for the operation."
			;;
		*)
			internal_error "Invalid mode for automount: $mode"
			;;
		esac
	fi

	for device in "${devices[@]}"; do
		if [[ -b "$device" ]]; then
			automount_used="y"
			device_mount "${devices[0]}" "$mount_path"
			return 0
		fi
	done
	[[ "$mode" = "fatal" ]] \
		&& fatal "No device available to be mounted!"
	return 1
}

# Internal function for other modules to use.
# It umounts our partition from $mount_path
autoumount() {
	[[ "${automount_used:-n}" = "y" ]] || return 0
	destination_umount "$mount_path"
	automount_used="n"
}
add_cleanup autoumount

# Mount operation ##############################################################
op_mount_usage() {
	echo "Usage: usbkey mount [DEVICE]" >&2
}

op_mount_usage_fail() {
	echo "usbkey:" "$@"
	op_mount_usage
	exit 2
}

op_mount_help() {
	op_mount_usage
	cat >&2 <<-EOF
		Mount currently plugged in key partition. It chooses the first one it
		encounters or the specified one.

		Options:
		  -p  Print mount point to stdout before exit
		  -h  Print this help text
	EOF
}

op_mount() {
	local print_mount="n"
	while getopts "hp" opt; do
		case "$opt" in
		h)
			op_mount_help
			exit 0
			;;
		p)
			print_mount="y"
			;;
		*)
			op_mount_usage_fail "Invalid option: $opt"
			;;
		esac
	done
	shift $((OPTIND - 1))
	[[ $# -le 1 ]] || {
		op_mount_usage_fail "Too many arguments provided: $#"
	}

	local device
	if [[ $# -eq 1 ]]; then
		device="$1"
		[[ -b "$device" ]] || fatal "Specified device not avilable: $device"
	else
		for dev in "${devices[@]}"; do
			if [[ -b "$dev" ]]; then
				device="$dev"
				break
			fi
		done
		[[ -n "${device:-}" ]] || fatal "None of the configured usb key devices could be located"
	fi
	device_mount "$device" "$mount_path"
	if [[ "$print_mount" == "y" ]]; then
		echo "$mount_path"
	fi
}

register_operation "mount" op_mount \
	"Mount key partition for later access"

# Umount operation #############################################################
op_umount_usage() {
	echo "Usage: usbkey umount [-a]" >&2
}

op_umount_usage_fail() {
	echo "usbkey:" "$@"
	op_umount_usage
	exit 2
}

op_umount_help() {
	op_umount_usage
	cat >&2 <<-EOF
		Umount currently mounted device. It unmounts the one that is mounted in
		'$mount_path' or all if -a is used.

		Options:
		  -a  Unmount all usb key devices
		  -h  Print this help text
	EOF
}

op_umount() {
	local all="n"
	while getopts "ah" opt; do
		case "$opt" in
		a)
			all="y"
			;;
		h)
			op_umount_help
			exit 0
			;;
		*)
			op_umount_usage_fail "Invalid option: $opt"
			;;
		esac
	done
	shift $((OPTIND - 1))
	[[ $# -eq 0 ]] || op_umount_usage_fail "No additional argument expected but got: $*"

	if [[ "$all" = "y" ]]; then
		for device in "${devices[@]}"; do
			device_umount "$device"
		done
	else
		destination_umount "$mount_path"
	fi
}

register_operation "umount" op_umount \
	"Umount previously mounted key partitions"

# TODO possibly add eject to really remove the device from the system
