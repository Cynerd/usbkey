# This implements helper git functions for other modules as well as operations
# used to access git with ease.
GIT_STASH_MESSAGE='USBKey automatic stash'

# Configuration options
declare -a git_args

# Git with configured arguments
_git() {
	git "${git_args[@]}" "$@"
}

# Initialize repository
git_initialize_repository() {
	local path="${1:-"$mount_path"}"
	_git init -b master "$path"
}

# Commit changes with provided message
# message: the message about changes performed
# path: path do the repository, in most cases that is "$mount_path"
# Any other argument has to be glob for files to be added.
git_commit_changes() {
	local message="$1"
	local path="$2"
	shift 2
	_git -C "$path" add "$@"
	_git -C "$path" commit -m "$message" -- "$@"
}

# Git inside git functions #####################################################
# Sometimes we want to push not just files to the usbkey but also git
# repositories of other tools. We can do it by creating merge commit without
# actually merging any files (strategy ours). This does two things. It recods
# hash of the latest commit of the pushed repository and also ties that commit
# to the usbkey history and thus prevents it from being garbage collected.

# Get hash of tip for latest previously pushed repository
git_repo_hash() {
	local ident="$1"
	local path="${2:-"$mount_path"}"
	local msg
	msg="$(_git -C "$path" log -1 --grep="^Repository: $ident:" --format=format:%s 2>/dev/null)"
	[[ -z "$msg" ]] \
		|| echo "${msg#"Repository: $ident: "}"
}

# Push given repository to usbkey repo
git_push_repo() {
	local ident="$1"
	local repo="$2"
	local path="${3:-"$mount_path"}"
	local newhsh
	newhsh="$(_git -C "$repo" rev-parse -q HEAD)"
	[[ -n "$newhsh" ]] || return 0
	[[ "$newhsh" != "$(git_repo_hash "$ident" "$path")" ]] || return 0

	if [[ -z "$(ls -A "$path/.git/refs/heads/")" ]]; then
		# Cover case when this is grand new repository and merge would result in
		# fast forward. New repository has no heads.
		_git -C "$path" commit -m "Initial commit" --allow-empty
	fi

	notice "Merging $ident: $repo ($newhsh)"
	_git -C "$path" fetch -q "$repo"
	_git -C "$path" merge --allow-unrelated-histories -s ours \
		--no-edit -m "Repository: $ident: $newhsh" \
		FETCH_HEAD
	# Note: this merge can't fail as it only joins history
}

# Pull or clone the repository originally pushed to usbkey
git_pull_repo() {
	local ident="$1"
	local repo="$2"
	local path="${3:-"$mount_path"}"
	local hsh
	hsh="$(git_repo_hash "$ident" "$path")"
	[[ -n "$hsh" ]] || return 0
	[[ "$(_git -C "$repo" rev-parse -q HEAD 2>/dev/null)" != "$hsh" ]] || return 0

	_git -C "$repo" fetch -q "$path"
	_git -C "$repo" merge "$hsh"
	# TODO what if merge fails
}

# Git repositories synchronization #############################################
# Syncronize the git repositories
# It first merges any changes from other repositories to the first one and then
# it resets head of it to be the same.
# To prevent any issues with untracked files the stash is used.
# Merge conflicts are requested to be resolved by the user.
git_sync_repositories() {
	[[ $# -gt 1 ]] || return 0 # do something only if there is at least two repos
	# TODO detect if we are not in the middle of merge and thus skip the stash
	_git_stash_uncommited "$@"
	_git_sync_repositories "$@"
	_git_stash_restore "$@"
}
_git_stash_uncommited() {
	for repo in "$@"; do
		[[ -z "$(_git -C "$repo" clean -xdn)" ]] \
			|| _git -C "$repo" stash push --all --message "$GIT_STASH_MESSAGE"
	done
}
_git_sync_repositories() {
	local primary="$1"
	shift
	for repo in "$@"; do
		_git -C "$repo" rev-parse -q --verify HEAD >/dev/null || continue
		_git -C "$primary" fetch -q "$repo"
		_git -C "$primary" merge FETCH_HEAD \
			|| fail "The merge failed of: $repo -> $primary"
		# TODO can we somehow help with resolving the conflicts?
	done
	_git -C "$primary" rev-parse -q --verify HEAD >/dev/null \
		|| return 0 # corner case on the grand new drives
	for repo in "$@"; do
		_git -C "$repo" fetch -q "$primary"
		_git -C "$repo" reset --hard FETCH_HEAD
	done
}
_git_stash_restore() {
	for repo in "$@"; do
		_git -C "$repo" stash list -1 | grep -qF "$GIT_STASH_MESSAGE" \
			|| continue
		_git -C "$repo" stash pop \
			|| notice "The stash restore failed on: $repo"
	done
}

# Git operation ################################################################
op_git() {
	automount "fatal"
	_git -C "$mount_path" "$@"
	autoumount
}
register_operation "git" op_git \
	"Run given Git command in the usbkey"

# Tig operation ################################################################
op_tig() {
	automount "fatal"
	tig -C "$mount_path" "$@"
	autoumount
}
register_operation "tig" op_tig \
	"Run given Tig command in the usbkey"

# Gitg operation ################################################################
op_gitg() (
	automount "fatal"
	cd "$mount_path" || internal_fatal "Unable to switch to: $mount_path"
	gitg "$@"
	autoumount
)
register_operation "gitg" op_gitg \
	"Run given Gitg command in the usbkey"
