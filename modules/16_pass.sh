# Password-store backup
# Notes about implementation here: The password store is using Git to manage the
# history and thus we can't just copy the files to our repository as that would
# break synchronization. We instead push git to the git repository and do merge
# to join histories.
pass_git_ident="Password store"

# Verify if there are any differnces between password store and usbkey
pass_check() {
	[[ -e ~/.password-store ]] || return 1
	local localhsh usbkeyhsh
	localhsh="$(git -C ~/.password-store rev-parse HEAD)"
	usbkeyhsh="$(git_repo_hash "$pass_git_ident")"
	[[ "$localhsh" = "$usbkeyhsh" ]] || return 1
	return 0
}

# Pull changes from the usbkey to the local password store
pass_pull() {
	if ! [[ -e ~/.password-store/.git ]]; then
		mkdir -p ~/.password-store
		git -C ~/.password-store init
	fi
	git_pull_repo "$pass_git_ident" ~/.password-store
}

# Push changes from the local password store to the usbkey
pass_push() {
	if [[ -e ~/.password-store/.git ]]; then
		git_push_repo "$pass_git_ident" ~/.password-store
	fi
}

# Pass operation ###############################################################
op_pass_usage() {
	echo "Usage: usbkey pass [OPTION].." >&2
}

op_pass_help() {
	op_pass_usage
	cat >&2 <<-EOF
		Store password store as part of USBKey.

		Options:
		  -d  Check if there is a difference between password store and the usbkey
		  -p  Only pull changes from usbkey to the local password store
		  -u  Only update (push changes) usbkey
	EOF
}

op_pass() {
	local check="n"
	local pull="y"
	local push="y"
	while getopts "dpuh" opt; do
		case "$opt" in
		d)
			check="y"
			;;
		p)
			push="n"
			;;
		u)
			pull="n"
			;;
		h)
			op_pass_help
			exit 0
			;;
		*)
			op_pass_usage
			exit 2
			;;
		esac
	done
	shift $((OPTIND - 1))
	[[ $# -eq 0 ]] || {
		echo "Invalid argument: $0" >&2
		op_pass_usage
		exit 2
	}

	local ec=0
	automount fatal

	if [[ "$check" = "y" ]]; then
		pass_check || ec=$?
	else
		[[ "$pull" = "y" ]] && pass_pull
		[[ "$push" = "y" ]] && pass_push
	fi

	autoumount
	return $ec
}

register_operation "pass" op_pass \
	"Password store synchronization to the USBKey"
