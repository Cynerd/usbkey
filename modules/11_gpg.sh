# GPG secrect key backup

gpg_list() {
	gpg --import-options show-only --import "$mount_path"/gpg/*.key
}

# Convert to the long fingerprint
gpg_fingerprint() {
	local keyid="$1"
	gpg --with-colons --fingerprint "$1" \
		| awk -F: '$1 == "fpr" { print $10; exit }'
}

gpg_export() {
	local fingerprint="${1:-}"
	while [[ -z "$fingerprint" ]]; do
		gpg --list-secret-keys
		local keyid
		read -p "KeyID: " -r keyid
		fingerprint="$(gpg_fingerprint "$keyid")" || true
	done

	mkdir -p "$mount_path/gpg"
	gpg --armor --export-secret-key "$fingerprint" \
		>"$mount_path/gpg/$fingerprint.key"
	git_commit_changes "Added GnuPG key '$fingerprint'" "$mount_path" \
		"$mount_path/gpg/$fingerprint.key"
}

gpg_update() {
	internal_error "Not implemented"
}

gpg_import() {
	local fingerprint="${1:-}"
	if [[ -z "$fingerprint" ]]; then
		echo "Available keys:"
		gpg_list
		while true; do
			read -r fingerprint -p "Select key: "
			[[ -f "$mount_path/gpg/$fingerprint.key" ]] && break
			notice "There is no key with fingerprint '$fingerprint'"
		done
	fi

	gpg --import "$mount_path/gpg/$fingerprint.key"
}

# GPG operation ################################################################
op_gpg_usage() {
	echo "Usage: ${0##*/} gpg [OPTION].. [FINGERPRINT].." >&2
}

op_gpg_help() {
	op_gpg_usage
	cat >&2 <<-EOF
		GnuPG secret key backup.

		Options:
		  -l  List available keys (on usbkey not in local gpg storage)
		  -n  Export new key from GnuPG to usbkey
		  -u  Update keys on the usbkey (handy when you add new subkeys)
	EOF
}

op_gpg() {
	local list="n"
	local action=gpg_import
	while getopts "lnuh" opt; do
		case "$opt" in
		l)
			list="y"
			;;
		n)
			action=gpg_export
			;;
		n)
			action=gpg_update
			;;
		h)
			op_gpg_help
			exit 0
			;;
		*)
			op_gpg_usage
			exit 2
			;;
		esac
	done
	shift $((OPTIND - 1))

	automount fatal

	if [[ "$list" = "y" ]]; then
		gpg_list
	else
		if [[ $# -gt 0 ]]; then
			for fingerprint in "$@"; do
				"$action" "$fingerprint"
			done
		else
			"$action"
		fi
	fi

	autoumount
}

register_operation "gpg" op_gpg \
	"GPG secret key backup"
