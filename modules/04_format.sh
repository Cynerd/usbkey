# This is the helper to prepare the new device.

# Format the new device.
# device: the device to be formated
# keep: the optional argument that should be "keep" so it mounts the device to
# the $mount_path and keeps it there.
format_new_device() {
	local device="$1"
	local keep="${2:-}"
	local destination
	if [[ "$keep" = "keep" ]]; then
		destination="$mount_path"
	else
		destination="$(mktemp -d)"
	fi

	notice "Formating '$device' using LUKS..."
	_sudo cryptsetup luksFormat "$device"

	notice "Openning new LUKS device..."
	local mapper
	mapper="usbkey-$(_sudo cryptsetup luksUUID "$device")"
	_sudo cryptsetup open "$device" "$mapper"

	notice "Formating new device with filesystem..."
	_sudo mkfs.exfat -n usbkey "/dev/mapper/$mapper"

	notice "Mounting new device to: $destination"
	_sudo mkdir -p "$destination"
	_sudo mount -o uid="$(id -u)",gid="$(id -g)" "/dev/mapper/$mapper" "$destination"

	notice "Initializing usb key store..."
	git_initialize_repository "$destination"

	if [[ "$keep" != "keep" ]]; then
		notice "Unmounting the new device..."
		_sudo umount "$destination"
		notice "Closing the new device..."
		sleep 1 # give some time to prevent device being busy
		_sudo cryptsetup close "$mapper"
	fi

	# Update list of our devices for the current run (so it can be used immediatelly)
	devices+=("$device")
}

# Format operation #############################################################
op_format_usage() {
	echo "Usage: usbkey format DEVICE" >&2
}

op_format_usage_fail() {
	echo "usbkey:" "$@"
	op_format_usage
	exit 2
}

op_format_help() {
	op_format_usage
	cat >&2 <<-EOF
		Format given device to be used as usb key device.

		Options:
		  -k  Keep mounted (keep the new device mounted)
		  -h  Print this help text
	EOF
}

op_format() {
	keep=""
	while getopts "kh" opt; do
		case "$opt" in
		k)
			keep="keep"
			;;
		h)
			op_format_help
			exit 0
			;;
		*)
			op_format_usage_fail "Invalid option: $opt"
			;;
		esac
	done
	shift $((OPTIND - 1))
	[[ $# -eq 1 ]] || op_format_usage_fail "Just one additional argument expected"
	local device="$1"
	[[ -b "$device" ]] || op_format_usage_fail "Provided path is not a device: $device"
	format_new_device "$device" "$keep"
}

register_operation "format" op_format \
	"Format a new drive"
