# vim: ft=sh

# WARNING: The defaults chosen here are not of production quality. The whole
# USBKey tool is intended for personal use and thus it rather tries to minimize
# the surprises than sticking to the best security practices.
# The result of this assessment is that validity used here is always 50 years.

# NOTE: there might be required even more versatility for other modules so do
# not afraid to touch this code!

# Check that there is certificate authority in the path
ca_auth_exists() {
	local path="$1"
	[[ -f "$path/ca.key" ]] && [[ -f "$path/ca.crt" ]]
}

# Generate new certificate authority
ca_initialize() {
	local config="$1"
	local path="$2"
	shift 2
	command -v openssl >/dev/null \
		|| fatal "OpenSSL application has to be available to generate the new key"
	ca_auth_exists "$path" \
		&& fatal "Certificate authority already exists. Refusing to overwrite."

	mkdir -p "$path"
	openssl req \
		-new -x509 \
		-config "$config" -days 18250 \
		-passout "pass:pass" \
		"$@" \
		-keyout "$path/ca.key" -out "$path/ca.crt"
}

# Check if there is certificate with given name.
ca_exists() {
	local path="$1"
	local name="$2"
	[[ -f "$path/$name.crt" ]]
}

# Create new certificate request and sign it with certificate authority.
ca_new() {
	local config="$1"
	local path="$2"
	local name="$3"
	shift 3
	command -v openssl >/dev/null \
		|| fatal "OpenSSL application has to be available to generate the new key"
	ca_auth_exists "$path" \
		|| fatal "Certificate authority is missing. Please generate one first."

	# TODO revoke the previous certificate!!!
	# TODO cover failure
	openssl req \
		-new \
		-config "$config" -days 18250 \
		-passout "pass:$name" \
		"$@" \
		-keyout "$path/$name.key" -out "$path/$name.csr"
	openssl ca \
		-config "$config" -days 18250 \
		-passin "pass:pass" -batch \
		-in "$path/$name.csr" -out "$path/$name.crt"
}

# Revoke existing certificate.
ca_revoke() {
	local config="$1"
	local path="$2"
	local name="$3"
	local reason="${4:-unspecified}"
	command -v openssl >/dev/null \
		|| fatal "OpenSSL application has to be available to generate the new key"
	ca_auth_exists "$path" \
		|| fatal "Certificate authority is missing. Please generate one first."
	ca_exists "$path" "$name" \
		|| fatal "There is no such certificate to revoke."

	openssl ca \
		-revoke "$path/$name.crt" \
		-config "$config" \
		-passin "pass:pass" -batch \
		-crl_reason "$reason"
}

# Generate certificate revocation list.
ca_crl() {
	local config="$1"
	local path="$2"
	local output="$3"
	command -v openssl >/dev/null \
		|| fatal "OpenSSL application has to be available to generate the new key"
	if ! [[ -f "$path/ca.key" ]] || ! [[ -f "$path/ca.crt" ]]; then
		fatal "Certificate authority is missing. Please generate one first."
	fi

	openssl ca \
		-gencrl \
		-config "$config" \
		-out "$output"
}
