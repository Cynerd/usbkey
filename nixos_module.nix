overlay: {
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cnf = config.programs.usbkey;
in {
  options = {
    programs.usbkey = {
      enable = mkOption {
        type = types.bool;
        default = false;
        description = "Enable USBKey configuration";
      };
      mountPath = mkOption {
        type = types.str;
        default = "/media/usbkey";
        description = "Standard path where device is mounted";
      };
      devicesUUID = mkOption {
        type = with types; listOf str;
        default = [];
        description = "UUIDs of the devices the USBKey should search for.";
      };
      devices = mkOption {
        type = with types; listOf str;
        default = [];
        description = "Devices the USBKey should search for.";
      };
      extraModules = mkOption {
        type = with types; listOf package;
        default = [];
        description = "Packages with extra modules for USBKey";
      };
      extraModulePaths = mkOption {
        type = with types; listOf path;
        default = [];
        description = "Paths where extra modules are located.";
      };
      package = mkOption {
        type = types.package;
        default = pkgs.usbkey;
        description = "Package to be used for USBKey";
      };
    };
  };

  config = mkIf cnf.enable {
    assertions = [
      {
        assertion = config.security.sudo.enable;
        message = "USBKey requires Sudo to work properly.";
      }
    ];

    nixpkgs.overlays = [overlay];
    environment.systemPackages = [cnf.package];

    programs.usbkey.extraModulePaths = map (v: "${v}/usr/share/usbkey") cnf.extraModules;

    environment.etc.usbkey.text = ''
      modules_dirs+=( ${concatMapStringsSep " " (p: "'${p}'") cnf.extraModulePaths} )
      : "${"$"}{mount_path:="${cnf.mountPath}"}"
      uuid_keys+=( ${concatMapStringsSep " " (p: "'${p}'") cnf.devicesUUID} )
      devices+=( ${concatMapStringsSep " " (p: "'${p}'") cnf.devices} )
    '';
  };
}
