# USBKey

This is shell script for maintaining keys on encrypted usb drive. The idea is
that you have one drive where you have all keys for all devices and services.
When you generate new key you do it on usb drive and then pull it to computer.
That way you should never ever have situation where you lost some key because
you wiped it out of pc. Of course when you generate key it's advised to
immediately backup usb drive to another drive which is also implemented in
USBKey.

Minimal dependencies:

* Bash with Core Utils
* Util-Linux
* Sudo
* Cryptsetup
* Exfat
* Git

Dependencies to get full functionality

* Tig (as alternative tool to work with Git)
* OpenSSL (for CA certificates)
* SSH (to generate SSH keys)
* GPG (to work with PGP keys)
* Wireguard (to generate wireguard keys)
* Syncthing (to generate Syncthing keys)


## Testing

There are tests implemented using bats. To run all tests just run:

```
bats tests
```
